cd /usr/src/tensorrt/bin
trtexec --onnx=/home/elephant/xzc_code/tensorrt_demos/onnx/ssd_resnet18_epoch_070.onnx --saveEngine=/home/elephant/xzc_code/tensorrt_demos/engine/ssd_resnet18_epoch_070_fp16.engine --fp16 
trtexec --onnx=/home/elephant/xzc_code/tensorrt_demos/onnx/int8/ssd_int8_resnet18_epoch_070.onnx --saveEngine=/home/elephant/xzc_code/tensorrt_demos/engine/ssd_resnet18_epoch_070_int8.engine --int8 --calib=/home/elephant/xzc_code/tensorrt_demos/onnx/int8/cal.bin

python python engine_test.py 


./trtexec --onnx=/home/jetson/tensorrt_demos/onnx/ssd_resnet18_epoch_070.onnx \
        --saveEngine=/home/jetson/tensorrt_demos/ssd_resnet18_epoch_070_fp16_2.engine \
        --fp16 